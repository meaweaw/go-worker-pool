package workerpool

type Dispatcher struct {
	workerPool chan chan Job
	maxWorkers int
	jobQueue   chan Job
}

func InitializeWorkerPool(maxWorkers int, maxQueue int) *Dispatcher {
	jobQueue := make(chan Job, maxQueue)
	dispatcher := NewDispatcher(jobQueue, maxWorkers)
	dispatcher.run()
	return dispatcher
}

func NewDispatcher(jobQueue chan Job, maxWorkers int) *Dispatcher {
	workerPool := make(chan chan Job, maxWorkers)

	return &Dispatcher{
		jobQueue:   jobQueue,
		maxWorkers: maxWorkers,
		workerPool: workerPool,
	}
}

func (d *Dispatcher) run() {
	for i := 0; i < d.maxWorkers; i++ {
		worker := NewWorker(i+1, d.workerPool)
		worker.start()
	}

	go d.dispatch()
}

func (d *Dispatcher) dispatch() {
	for {
		select {
		case job := <-d.jobQueue:
			go func() {
				workerJobQueue := <-d.workerPool
				workerJobQueue <- job
			}()
		}
	}
}

func AddJobToQueue(d *Dispatcher, job Job) {
	if job.Process == nil || job.Payload == nil {
		return
	}
	d.jobQueue <- job
}
