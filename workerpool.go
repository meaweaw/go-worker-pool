package workerpool

import (
	"fmt"
)

type Job struct {
	Payload any
	Process func()
}

type Worker struct {
	id         int
	jobQueue   chan Job
	workerPool chan chan Job
	quitChan   chan bool
}

func NewWorker(id int, workerPool chan chan Job) Worker {
	return Worker{
		id:         id,
		jobQueue:   make(chan Job),
		workerPool: workerPool,
		quitChan:   make(chan bool),
	}
}

func (w Worker) start() {
	go func() {
		for {
			// Add my jobQueue to the worker pool.
			w.workerPool <- w.jobQueue

			select {
			case job := <-w.jobQueue:
				// Dispatcher has added a job to my jobQueue and process.
				fmt.Printf("worker %d : started process\n", w.id)
				job.Process()
				fmt.Printf("worker %d : finished process\n", w.id)
			case <-w.quitChan:
				// We have been asked to stop.
				fmt.Printf("worker %d : stopping\n", w.id)
				return
			}
		}
	}()
}

func (w Worker) stop() {
	go func() {
		w.quitChan <- true
	}()
}
